using System.Reflection;
using GreenHaven.Parties.Model.Parties;
using GreenHaven.Parties.Model.Role;
using GreenHaven.Parties.Utils;

namespace GreenHaven.Parties.Api;

public class RoleObjectFactory
{
    private readonly PolymorphicDictionary<PartyBasedRole> _roles = new();

    public bool HasRole<T>() where T : PartyBasedRole
    {
        return _roles.ContainsKey(typeof(T));
    }

    public static RoleObjectFactory From(PartyRelationship partyRelationship)
    {
        var roleObject = new RoleObjectFactory();
        roleObject.Add(partyRelationship);
        return roleObject;
    }

    private void Add(PartyRelationship partyRelationship)
    {
        Add(partyRelationship.RoleA, partyRelationship.PartyA);
        Add(partyRelationship.RoleB, partyRelationship.PartyB);
    }

    private void Add(string role, Party party)
    {
        try
        {
            //in sake of simplicity: a role name is same as a class name with no mapping between them
            var type = Assembly.GetAssembly(GetType()).DefinedTypes.Single(t => t.Name == role);
            var instance = (PartyBasedRole)Activator.CreateInstance(type, party);
            _roles[type] = instance;
        }
        catch (Exception e)
        {
            throw new ArgumentException("invalid role", nameof(role), e);
        }
    }

    public T? GetRole<T>() where T : PartyBasedRole
    {
        return (T?)_roles[typeof(T)];
    }
}
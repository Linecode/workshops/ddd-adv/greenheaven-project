using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Parties.Api;

public class PartyMapper(IPartyRelationshipRepository partyRelationshipRepository)
{
    public Task<PartyRelationship?> MapRelation(PartyId id, string relationshipName)
    {
        return partyRelationshipRepository.FindRelationshipFor(id, relationshipName);
    }
}
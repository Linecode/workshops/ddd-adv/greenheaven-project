using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Parties.Infrastructure;

public class InMemoryPartyRepository : Dictionary<Guid, Party>, IPartyRepository
{
    public Task<Party> Save(Guid id)
    {
        TryGetValue(id, out var party);

        if (party is null)
        {
            party = new Party
            {
                Id = id
            };
            Add(id, party);
        }
        
        return Task.FromResult(party);
    }
}
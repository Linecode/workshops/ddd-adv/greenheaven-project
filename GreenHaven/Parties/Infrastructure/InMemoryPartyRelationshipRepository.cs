using GreenHaven.Parties.Api;
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Parties.Infrastructure;

public class InMemoryPartyRelationshipRepository : List<PartyRelationship>, IPartyRelationshipRepository
{
    public Task<PartyRelationship> Save(string partyRelationship,
        string partyARole,
        Party partyA,
        string partyBRole,
        Party partyB)
    {
        PartyRelationship relationship;
        var parties = this
            .Where(x => x.Name == partyRelationship &&
                        ((x.PartyA.Id == partyA.Id && x.PartyB.Id == partyB.Id) ||
                         (x.PartyA.Id == partyB.Id && x.PartyB.Id == partyA.Id)))
            .ToList();

        if (!parties.Any())
        {
            relationship = new PartyRelationship();
            Add(relationship);
        }
        else
        {
            relationship = parties.First();
        }
        
        relationship.Name = partyRelationship;
        relationship.PartyA = partyA;
        relationship.PartyB = partyB;
        relationship.RoleA = partyARole;
        relationship.RoleB = partyBRole;

        return Task.FromResult(relationship);
    }

    public Task<PartyRelationship?> FindRelationshipFor(PartyId id, string relationshipName)
    {
        var parties = this.Where(x =>
            x.Name == relationshipName && (x.PartyA.Id == id.ToGuid() || x.PartyB.Id == id.ToGuid())).ToList();
        
        return Task.FromResult(parties.FirstOrDefault());
    }
}
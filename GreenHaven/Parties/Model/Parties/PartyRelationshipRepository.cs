using GreenHaven.Parties.Api;

namespace GreenHaven.Parties.Model.Parties;

public interface IPartyRelationshipRepository
{
    Task<PartyRelationship> Save(
        string partyRelationship,
        string partyARole,
        Party partyA,
        string partyBRole,
        Party partyB);

    Task<PartyRelationship?> FindRelationshipFor(PartyId id, string relationshipName);
}
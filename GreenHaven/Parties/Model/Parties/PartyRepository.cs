namespace GreenHaven.Parties.Model.Parties;

public interface IPartyRepository
{
    Task<Party> Save(Guid id);
}
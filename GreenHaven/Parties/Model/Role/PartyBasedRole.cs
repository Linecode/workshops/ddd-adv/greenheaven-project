using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Parties.Model.Role;

public abstract class PartyBasedRole
{
    protected Party Party;

    protected PartyBasedRole(Party party)
    {
        Party = party;
    }
}
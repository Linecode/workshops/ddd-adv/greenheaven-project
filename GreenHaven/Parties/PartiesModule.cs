using GreenHaven.Parties.Api;
using GreenHaven.Parties.Infrastructure;
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Parties;

public abstract class PartiesModule {}

public static class PartiesModuleExtensions
{
    public static IServiceCollection AddPartiesModule(this IServiceCollection services)
    {
        services.AddTransient<PartyMapper>();
        services.AddSingleton<IPartyRepository, InMemoryPartyRepository>();
        services.AddSingleton<IPartyRelationshipRepository, InMemoryPartyRelationshipRepository>();
        
        return services;
    }
}
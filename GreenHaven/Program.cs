using System.Text.Json;
using System.Text.Json.Serialization;
using FastEndpoints;
using FastEndpoints.Swagger;
using GreenHaven.Common;
using GreenHaven.Franchise.Agreements;
using GreenHaven.Parties;
using GreenHaven.Restaurants.BusinessProcess.RestaurantPreparation;
using GreenHaven.Restaurants.Dishes;
using GreenHaven.Restaurants.Orders;
using OpenSleigh.Core.DependencyInjection;
using OpenSleigh.Persistence.InMemory;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddFastEndpoints(o =>
{
    o.IncludeAbstractValidators = true;
});
builder.Services.SwaggerDocument(o =>
{
    o.DocumentSettings = s =>
    {
        s.Title = "PixelPlayInc Api";
        s.Version = "v1";
    };
});

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblyContaining<OrdersModule>());

builder.Services
    .AddCommonModule()
    .AddPartiesModule()
    .AddAgreementsModule()
    .AddOrdersModule()
    .AddDishesModule();

builder.Services.AddOpenSleigh(cfg =>
{
    cfg.UseInMemoryTransport()
        .UseInMemoryPersistence();

    cfg.AddSaga<RestaurantPreparationSaga, RestaurantPreparationSagaState>()
        .UseStateFactory<StartRestaurantPreparationCommand>(
            msg => new RestaurantPreparationSagaState(msg.CorrelationId))
        .UseInMemoryTransport();
});

var app = builder.Build();

app.UseHttpsRedirection();

app.UseFastEndpoints(c =>
{
    c.Serializer.Options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    c.Endpoints.RoutePrefix = "api";
    c.Serializer.Options.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
});
app.UseSwaggerGen();

app.Run();
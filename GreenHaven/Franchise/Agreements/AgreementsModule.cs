using GreenHaven.Franchise.Agreements.Api;

namespace GreenHaven.Franchise.Agreements;

public abstract class AgreementsModule {}

public static class AgreementsModuleExtensions
{
    public static IServiceCollection AddAgreementsModule(this IServiceCollection services)
    {
        services.AddScoped<IAgreementsContractManager, AgreementsContractManager>();

        return services;
    }
}
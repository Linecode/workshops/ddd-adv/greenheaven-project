using GreenHaven.Parties.Api;

namespace GreenHaven.Franchise.Agreements.Api;

public interface IAgreementsContractManager
{
    Task ContractAgreementsSigned(PartyId franchisorId, PartyId franchiseeId);
    Task BusinessAgreementSigned(PartyId franchisorId, PartyId franchiseeId);
}
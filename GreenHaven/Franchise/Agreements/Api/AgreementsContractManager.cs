using GreenHaven.Franchise.Agreements.Domain.Dict;
using GreenHaven.Parties.Api;
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Franchise.Agreements.Api;

public class AgreementsContractManager(IPartyRepository partyRepository,
    IPartyRelationshipRepository partyRelationshipRepository) : IAgreementsContractManager
{
    public async Task ContractAgreementsSigned(PartyId franchisorId, PartyId franchiseeId)
    {
        var franchisor = await partyRepository.Save(franchisorId.ToGuid());
        var franchisee = await partyRepository.Save(franchiseeId.ToGuid());

        await partyRelationshipRepository.Save(PartyRelationshipDictionary.Contract.ToString(), 
            PartyRolesDictionary.Contracting.RoleName, franchisor, 
            PartyRolesDictionary.Contractor.RoleName, franchisee);        
    }

    public async Task BusinessAgreementSigned(PartyId franchisorId, PartyId franchiseeId)
    {
        var franchisor = await partyRepository.Save(franchisorId.ToGuid());
        var franchisee = await partyRepository.Save(franchiseeId.ToGuid());
        
        await partyRelationshipRepository.Save(PartyRelationshipDictionary.Contract.ToString(), 
            PartyRolesDictionary.Supporting.RoleName, franchisor, 
            PartyRolesDictionary.Supported.RoleName, franchisee);
    }
}
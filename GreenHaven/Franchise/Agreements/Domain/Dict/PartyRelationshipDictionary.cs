namespace GreenHaven.Franchise.Agreements.Domain.Dict;

public enum PartyRelationshipDictionary
{
    Contract,
    Business,
    Marketing
}
using GreenHaven.Franchise.Agreements.Domain.Roles.Business;
using GreenHaven.Franchise.Agreements.Domain.Roles.Contract;
using GreenHaven.Parties.Model.Role;

namespace GreenHaven.Franchise.Agreements.Domain.Dict;

public static class PartyRolesDictionary
{
    public static PartyRolesDictionary<Contractor> Contractor => new(nameof(Contractor));
    public static PartyRolesDictionary<Contracting> Contracting => new(nameof(Contracting));
    public static PartyRolesDictionary<Supported> Supported  => new(nameof(Supported));
    public static PartyRolesDictionary<Supporting> Supporting => new(nameof(Supporting));
}

public sealed record PartyRolesDictionary<T> where T : PartyBasedRole
{
    private readonly string _memberName;

    public PartyRolesDictionary(string memberName)
    {
        _memberName = memberName;
    }

    public string RoleName => typeof(T).Name;

    public override string ToString()
    {
        return _memberName;
    }
}
using GreenHaven.Parties.Model.Parties;
using GreenHaven.Parties.Model.Role;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Contract;

// Could be also in application layer
public abstract class RoleForContract : PartyBasedRole
{
    public RoleForContract(Party party) : base(party)
    {
    }
}
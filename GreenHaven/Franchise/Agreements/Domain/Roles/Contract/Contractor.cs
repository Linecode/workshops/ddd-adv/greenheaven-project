using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Contract;

public class Contractor : RoleForContract
{
    public Contractor(Party party) : base(party)
    {
    }
}
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Contract;

public class Contracting : RoleForContract
{
    public Contracting(Party party) : base(party)
    {
    }
}
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Business;

public class Supported : RoleForBusiness
{
    public Supported(Party party) : base(party)
    {
    }
}
using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Business;

public class Supporting : RoleForBusiness
{
    public Supporting(Party party) : base(party)
    {
    }
}
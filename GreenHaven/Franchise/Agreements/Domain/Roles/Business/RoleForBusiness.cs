using GreenHaven.Parties.Model.Parties;
using GreenHaven.Parties.Model.Role;

namespace GreenHaven.Franchise.Agreements.Domain.Roles.Business;

// Could be also in application layer
public class RoleForBusiness : PartyBasedRole
{
    public RoleForBusiness(Party party) : base(party)
    {
    }
}
using FastEndpoints;
using GreenHaven.Franchise.Agreements.Api;
using GreenHaven.Parties.Api;

namespace GreenHaven.Franchise.Agreements.Presentation.Rest;

public class SignContractAgreement(IAgreementsContractManager agreementsContractManager) 
    : Endpoint<SignContractAgreementRequest>
{
    public override void Configure()
    {
        Post("/agreements/sign");
        AllowAnonymous();
    }

    public override async Task HandleAsync(SignContractAgreementRequest req, CancellationToken ct)
    {
        var franchisorId = new PartyId(req.Franchisor);
        var franchiseeId = new PartyId(req.Franchisee);

        await agreementsContractManager.ContractAgreementsSigned(franchisorId, franchiseeId);

        await SendAsync(null, 200, ct);
    }
}

public enum AgreementType { Contract, Business }
public record SignContractAgreementRequest(Guid Franchisor, Guid Franchisee, AgreementType Type);
using GreenHaven.Transactions.Api.Ports;
using GreenHaven.Transactions.Domain;

namespace GreenHaven.Transactions.Infrastructure.Db.InMemory;

public class InMemoryTransactionRepository : Dictionary<Guid, Transaction>, ITransactionRepository
{
    public Task<Transaction> GetAsync(Guid id)
    {
        return Task.FromResult(this[id]);
    }

    public Task SaveAsync(Transaction transaction)
    {
        Add(transaction.Id, transaction);
        
        return Task.CompletedTask;
    }
}
using GreenHaven.BuildingBlocks;

namespace GreenHaven.Transactions.Domain;

public class Money : ValueObject<Money>
{
    public decimal Amount { get; }

    private Money(decimal amount)
    {
        Amount = amount;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Amount;
    }

    public static Money New(decimal amount)
    {
        if (amount < 0) 
            throw new ArgumentOutOfRangeException(nameof(amount), amount, "Amount cannot be negative");
        
        return new(amount);
    }
}
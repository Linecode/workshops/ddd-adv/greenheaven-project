using GreenHaven.BuildingBlocks;
using Microsoft.AspNetCore.Authentication.BearerToken;

namespace GreenHaven.Transactions.Domain;

public class Transaction : Entity<Guid>, AggregateRoot<Guid>
{
    public enum Statues { Initialized, Completed, Reverted }
    
    public Money Amount { get; private set; }
    
    public Guid SenderId { get; private set; }
    public Guid ReceiverId { get; private set; }

    public Statues Status { get; private set; } = Statues.Initialized;

    public Transaction(Money amount, Guid senderId, Guid receiverId)
    {
        Amount = amount;
        SenderId = senderId;
        ReceiverId = receiverId;
    }

    public IEnumerable<Events> Complete()
    {
        Status = Statues.Completed;
        
        var @event = new Events.TransactionCompleted(Id, Amount, SenderId, ReceiverId);

        return new [] {@event};
    }

    public void ChangeMoney(Money amount)
    {
        Amount = amount;
    }

    public abstract record Events
    {
        public record TransactionCompleted(Guid TransactionId, Money Amount, Guid SenderId, Guid ReceiverId) : Events;
    }
}
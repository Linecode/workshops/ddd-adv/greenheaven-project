using FastEndpoints;
using GreenHaven.Transactions.Api;
using GreenHaven.Transactions.Domain;

namespace GreenHaven.Transactions.Presentation;

// public class TransactionCreate : Endpoint<CreateTransactionRequest>
// {
//     private readonly TransactionFacade _transactionFacade;
//
//     public TransactionCreate(TransactionFacade transactionFacade)
//     {
//         _transactionFacade = transactionFacade;
//     }
//     
//     public override void Configure()
//     {
//         Post("/transactions");
//         AllowAnonymous();
//     }
//
//     public override async Task HandleAsync(CreateTransactionRequest request, CancellationToken cancellationToken)
//     {
//         var command = new TransactionCommand.Create(
//             Money.New(request.Amount), 
//             request.SenderId, 
//             request.ReceiverId);
//         
//         await _transactionFacade.CreateAsync(command);
//         
//         await SendAsync("OK", 200, cancellationToken);
//     }
// }

public record CreateTransactionRequest(decimal Amount, Guid SenderId, Guid ReceiverId);
using GreenHaven.Transactions.Api;
using GreenHaven.Transactions.Api.Ports;
using GreenHaven.Transactions.Infrastructure.Db.InMemory;

namespace GreenHaven.Transactions;

public abstract class TransactionModule
{
}

public static class TransactionModuleExtensions
{
    public static IServiceCollection AddTransactions(this IServiceCollection services)
    {
        services.AddScoped<TransactionFacade>();
        services.AddSingleton<ITransactionRepository, InMemoryTransactionRepository>();
        
        return services;
    }
}
using GreenHaven.BuildingBlocks;
using GreenHaven.Transactions.Api.Ports;
using GreenHaven.Transactions.Domain;

namespace GreenHaven.Transactions.Api;

[ApplicationService]
public class TransactionFacade
{
    private readonly ITransactionRepository _repository;
    private readonly IBankAccountRepository _bankAccountRepository;
    private readonly IEventsPublisher _eventsPublisher;

    public TransactionFacade(ITransactionRepository repository, 
        IBankAccountRepository bankAccountRepository, 
        IEventsPublisher eventsPublisher)
    {
        _repository = repository;
        _bankAccountRepository = bankAccountRepository;
        _eventsPublisher = eventsPublisher;
    }

    public async Task CreateAsync(TransactionCommand.Create command)
    {
        var (amount, senderId, receiverId) = command;
        
        var transaction = new Transaction(amount, senderId, receiverId);
        
        transaction.ChangeMoney(Money.New(30));

        await _repository.SaveAsync(transaction);
    }

    public async Task Complete()
    {
        var transaction = await _repository.GetAsync(Guid.NewGuid());

        var events = transaction.Complete();
        
        await _repository.SaveAsync(transaction);
        
        // Transaction?? 
        
        await _eventsPublisher.PublishAsync(events);
        // var sender = await _bankAccountRepository.Get(transaction.SenderId);
        // var receiver = await _bankAccountRepository.Get(transaction.ReceiverId);
        //
        // sender.ChangeMoney(transaction.Amount);
        // receiver.ChangeMoney(transaction.Amount);
        //
        // await _repository.SaveAsync(transaction);
        // await _bankAccountRepository.SaveAsync(sender);
        // await _bankAccountRepository.SaveAsync(receiver);

        // await _uot.SaveChanges();
    }
}

public record TransactionCommand
{
    public record Create(Money Amount, Guid SenderId, Guid ReceiverId) : TransactionCommand;
}
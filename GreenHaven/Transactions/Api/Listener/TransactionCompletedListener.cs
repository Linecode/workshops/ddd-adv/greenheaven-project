using MediatR;

namespace GreenHaven.Transactions.Api.Listener;

public class TransactionCompletedListener : INotificationHandler<TransactionCompletedEvent>
{
    public Task Handle(TransactionCompletedEvent notification, CancellationToken cancellationToken)
    {
        // DI: BankAccountFacade
        // bankAccountFacade.Process(notification.Transaction);
        
        return Task.CompletedTask;
    }
}

public record TransactionCompletedEvent : INotification;
using GreenHaven.Transactions.Domain;

namespace GreenHaven.Transactions.Api.Ports;

public interface IBankAccountRepository
{
    Task<BankAccount> Get(Guid id);
    Task SaveAsync(BankAccount sender);
}
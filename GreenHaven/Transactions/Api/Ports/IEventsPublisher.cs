namespace GreenHaven.Transactions.Api.Ports;

public interface IEventsPublisher
{
    Task PublishAsync(IEnumerable<object> events);
}
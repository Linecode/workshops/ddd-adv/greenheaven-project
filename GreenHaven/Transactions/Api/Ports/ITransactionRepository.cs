using GreenHaven.Transactions.Domain;

namespace GreenHaven.Transactions.Api.Ports;

public interface ITransactionRepository
{
    Task<Transaction> GetAsync(Guid id);
    Task SaveAsync(Transaction transaction);
}
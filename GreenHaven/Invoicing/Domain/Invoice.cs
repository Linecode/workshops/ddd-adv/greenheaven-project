using GreenHaven.BuildingBlocks;
using GreenHaven.Invoicing.Infratrcture;
using GreenHaven.Transactions.Domain;

namespace GreenHaven.Invoicing.Domain;


public class UnPaidInvoice : Entity<Guid>, AggregateRoot<Guid>
{
    public void Pay()
    {
        
    }
}

public class PastDueInvoice : Entity<Guid>, AggregateRoot<Guid>
{
    public void Smth()
    {
        
    }
}

public class InvoiceHeader
{
    
}

public class InvoiceLineV2 : Entity<Guid>, AggregateRoot<Guid>
{
    public Guid InvoiceId { get; }
}

public class Invoice : Entity<Guid>, AggregateRoot<Guid>
{
    public LegalEntity Sender { get; }
    public LegalEntity Receiver { get; }
    
    public DateTime IssuedOn { get; private set; }
    public DateTime DueDate { get; private set; }
    
    private readonly List<InvoiceLine> _lines = new();
    public IReadOnlyList<InvoiceLine> Lines => _lines;

    public Money TotalAmount()
    {
        return Money.New(_lines.Sum(x => x.TotalPrice.Amount));
    }
    
    // Opłacona / nieopłacona / zaległa / zaksięgowana / 

    public void Pay()
    {
        // if (Status != InvoiceEntity.Statuses.Draft)
        //     throw new InvalidOperationException("Invoice is not in draft status");
    }

    public Invoice(LegalEntity sender, LegalEntity receiver, DateTime issuedOn, DateTime dueDate)
    {
        Sender = sender;
        Receiver = receiver;
        IssuedOn = issuedOn;
        DueDate = dueDate;
    }
}

public class LegalEntity : Entity<Nip>
{
    public string Name { get; }
    public string Address { get; }
}

public class InvoiceLine : ValueObject<InvoiceLine>
{
    public string Name { get; }
    public int Quantity { get; }
    public Money Price { get; }
    public TaxType TaxType { get; }

    public InvoiceLine(string name, int quantity, Money price, TaxType taxType)
    {
        Name = name;
        Quantity = quantity;
        Price = price;
        TaxType = taxType;
    }

    public Money TotalPrice => Money.New(Price.Amount * Quantity * TaxType.Rate);
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        throw new NotImplementedException();
    }
}

public class TaxType : ValueObject<TaxType>
{
    public decimal Rate { get; }

    public TaxType(decimal rate)
    {
        Rate = rate;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Rate;
    }
}

public class Nip : ValueObject<Nip>
{
    public string Value { get; }
    
    public Nip(string value)
    {
        Value = value;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}


namespace GreenHaven.Common;

public abstract class CommonModule
{
}

public static class CommonModuleExtensions
{
    public static IServiceCollection AddCommonModule(this IServiceCollection services)
    {
        services.AddTransient<IDateTimeProvider, DateTimeProvider>();
        
        return services;
    }
}
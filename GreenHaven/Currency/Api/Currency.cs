using GreenHaven.BuildingBlocks;

namespace GreenHaven.Currency.Api;

/// <summary>
///     A mapping of distinct ISO 4217 currency code and numeric code
/// </summary>
public class Currency : ValueObject<Currency>
{
    public decimal MajorUnitToMinorUnitDivisor { get; init; }
    public string Definition { get; init; }
    public string AlphabeticCode { get; init; }
    public int NumericCode { get; init; }
    public string MajorUnitSymbol { get; init; }
    public string MinorUnitSymbol { get; init; }

    private Currency(decimal majorUnitToMinorUnitDivisor, string definition, string alphabeticCode, int numericCode,
        string majorUnitSymbol, string minorUnitSymbol)
    {
        MajorUnitToMinorUnitDivisor = majorUnitToMinorUnitDivisor;
        Definition = definition;
        AlphabeticCode = alphabeticCode;
        NumericCode = numericCode;
        MajorUnitSymbol = majorUnitSymbol;
        MinorUnitSymbol = minorUnitSymbol;
    }
    
    private static Currency New(decimal majorUnitToMinorUnitDivisor, string definition, string alphabeticCode,
        int numericCode, string majorUnitSymbol, string minorUnitSymbol)
    {
        if (majorUnitToMinorUnitDivisor < 0)
            throw new ArgumentException("Major unit to minor unit divisor incorrect");

        if (string.IsNullOrEmpty(alphabeticCode))
            throw new ArgumentException("Alphabetic code incorrect");

        var currency = new Currency(majorUnitToMinorUnitDivisor,
            definition,
            alphabeticCode,
            numericCode,
            majorUnitSymbol,
            minorUnitSymbol);

        return currency;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Definition;
        yield return AlphabeticCode.ToLower();
        yield return NumericCode;
        yield return MajorUnitSymbol;
        yield return MinorUnitSymbol;
    }
    
    // Static properties to simulate a db table
    public static Currency None => New(decimal.Zero, string.Empty, string.Empty, 0, string.Empty, string.Empty);
    public static Currency Eur => New(100, string.Empty, "EUR", 978, "€", "c");
    public static Currency Dol => New(100, string.Empty, "DOL", 840, "$", string.Empty);
    public static Currency Jpy => New(1, string.Empty, "YEN", 392, "¥", string.Empty);
    public static Currency Pol => New(100, string.Empty, "PLN", 985, "zł", string.Empty);

}

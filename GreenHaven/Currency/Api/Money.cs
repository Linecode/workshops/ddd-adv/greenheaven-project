using GreenHaven.BuildingBlocks;

namespace GreenHaven.Currency.Api;

public sealed class Money : ValueObject<Money>
{
    public decimal Amount { get; }
    public Currency Currency { get; }

    private Money(decimal amount, Currency currency)
    {
        if (currency == null)
            throw new ArgumentException("Currency incorrect");

        Amount = amount;
        Currency = currency;
    }
    
    public static Money New(decimal amount, Currency currency) => new(amount, currency);

    public static Money Zero(Currency currency) => new(Decimal.Zero, currency);

    public static Money operator +(Money a, Money b)
    {
        ArgumentNullException.ThrowIfNull(a);
        ArgumentNullException.ThrowIfNull(b);

        return new Money(a.Amount + b.Amount, a.Currency);
    }

    public static Money operator -(Money a, Money b)
    {
        ArgumentNullException.ThrowIfNull(a);
        ArgumentNullException.ThrowIfNull(b);

        return new Money(a.Amount - b.Amount, a.Currency);
    }

    public static Money operator *(Money a, decimal b)
    {
        ArgumentNullException.ThrowIfNull(a);

        var amount = a.Amount * b;

        return new Money(amount, a.Currency);
    }

    public static bool operator <=(Money a, Money b)
    {
        if (a.Currency != b.Currency)
            throw new InvalidOperationException("Cannot compare different currencies");
        
        return a.Amount <= b.Amount;
    }

    public static bool operator >=(Money a, Money b)
    {
        if (a.Currency != b.Currency)
            throw new InvalidOperationException("Cannot compare different currencies");
        
        return a.Amount >= b.Amount;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Amount;
        yield return Currency;
    }
}

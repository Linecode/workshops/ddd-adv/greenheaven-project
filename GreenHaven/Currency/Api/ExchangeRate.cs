using GreenHaven.BuildingBlocks;
using GreenHaven.Common;

namespace GreenHaven.Currency.Api;

public sealed class ExchangeRate : ValueObject<ExchangeRate>
{
    public decimal Rate { get; private set; }
    public Currency FromCurrency { get; private set; }
    public Currency ToCurrency { get; private set; }
    public DateRange ValidTimeFrame { get; private set; }

    public static ExchangeRate New(decimal rate, Currency fromCurrency, Currency toCurrency, DateRange validTimeFrame) => new()
    {
        Rate = rate,
        FromCurrency = fromCurrency,
        ToCurrency = toCurrency,
        ValidTimeFrame = validTimeFrame,
    };

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Rate;
        yield return FromCurrency;
        yield return ToCurrency;
        yield return ValidTimeFrame;
    }
}

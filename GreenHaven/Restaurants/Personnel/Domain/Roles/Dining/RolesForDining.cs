using GreenHaven.Parties.Model.Parties;
using GreenHaven.Parties.Model.Role;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Dining;

public class RolesForDining : PartyBasedRole
{
    public RolesForDining(Party party) : base(party)
    {
    }
}
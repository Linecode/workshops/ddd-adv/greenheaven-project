using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Dining;

public class Waiter : RolesForDining
{
    public Waiter(Party party) : base(party)
    {
    }
}
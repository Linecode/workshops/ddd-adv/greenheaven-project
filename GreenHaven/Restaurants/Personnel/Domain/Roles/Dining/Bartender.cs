using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Dining;

public class Bartender : RolesForDining
{
    public Bartender(Party party) : base(party)
    {
    }
}
using GreenHaven.Parties.Model.Parties;
using GreenHaven.Parties.Model.Role;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Kitchen;

public abstract class RoleForKitchen : PartyBasedRole
{
    public RoleForKitchen(Party party) : base(party)
    {
    }
}
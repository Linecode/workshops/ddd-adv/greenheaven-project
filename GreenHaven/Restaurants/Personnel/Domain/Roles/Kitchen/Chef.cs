using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Kitchen;

public class Chef : RoleForKitchen
{
    public Chef(Party party) : base(party)
    {
    }
}
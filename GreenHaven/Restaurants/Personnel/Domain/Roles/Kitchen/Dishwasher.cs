using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Kitchen;

public class Dishwasher : RoleForKitchen
{
    public Dishwasher(Party party) : base(party)
    {
    }
}
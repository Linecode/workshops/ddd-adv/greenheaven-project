using GreenHaven.Parties.Model.Parties;

namespace GreenHaven.Restaurants.Personnel.Domain.Roles.Kitchen;

public class KitchenPorter : RoleForKitchen
{
    public KitchenPorter(Party party) : base(party)
    {
    }
}
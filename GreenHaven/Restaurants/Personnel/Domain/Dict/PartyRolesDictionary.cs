using GreenHaven.Parties.Model.Role;
using GreenHaven.Restaurants.Personnel.Domain.Roles.Dining;
using GreenHaven.Restaurants.Personnel.Domain.Roles.Kitchen;

namespace GreenHaven.Restaurants.Personnel.Domain.Dict;

public static class PartyRolesDictionary
{
    public static PartyRolesDictionary<Waiter> Waiter => new(nameof(Waiter));
    public static PartyRolesDictionary<Bartender> Bartender => new(nameof(Bartender));
    public static PartyRolesDictionary<Chef> Chef => new(nameof(Chef));
    public static PartyRolesDictionary<Dishwasher> Dishwasher => new(nameof(Dishwasher));
    public static PartyRolesDictionary<KitchenPorter> Kitchen => new(nameof(KitchenPorter));
}

public sealed record PartyRolesDictionary<T> where T : PartyBasedRole
{
    private readonly string _memberName;

    public PartyRolesDictionary(string memberName)
    {
        _memberName = memberName;
    }

    public string RoleName => typeof(T).Name;

    public override string ToString()
    {
        return _memberName;
    }
}
using GreenHaven.BuildingBlocks;

namespace GreenHaven.Restaurants.Dishes.Domain;

public sealed class Recipe : Entity<Guid>, AggregateRoot<Guid>
{
    public enum ComplexityLevel { Easy, Medium, Hard }
    public enum State { Draft, Published }

    public Cuisine Cuisine { get; private set; }

    public ComplexityLevel Complexity { get; private set; }

    public State Status { get; private set; } = State.Draft;

    public Recipe(Cuisine cuisine, ComplexityLevel complexity)
    {
        Id = Guid.NewGuid();
        Cuisine = cuisine;
        Complexity = complexity;
    }

    public Recipe(Guid id, Cuisine cuisine, ComplexityLevel complexity)
    {
        Id = id;
        Cuisine = cuisine;
        Complexity = complexity;
    }
}
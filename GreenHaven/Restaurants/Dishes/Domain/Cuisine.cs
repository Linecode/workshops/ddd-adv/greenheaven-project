using Ardalis.SmartEnum;

namespace GreenHaven.Restaurants.Dishes.Domain;

public sealed class Cuisine : SmartEnum<Cuisine>
{
    public static readonly Cuisine Italian = new(nameof(Italian), 1);
    public static readonly Cuisine French = new(nameof(French), 2);
    
    public Cuisine(string name, int value) : base(name, value)
    {
    }
}
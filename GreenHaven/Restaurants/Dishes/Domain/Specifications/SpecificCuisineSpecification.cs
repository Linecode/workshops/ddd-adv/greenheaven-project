using System.Linq.Expressions;
using GreenHaven.BuildingBlocks;

namespace GreenHaven.Restaurants.Dishes.Domain.Specifications;

public class SpecificCuisineSpecification : Specification<Recipe>
{
    private readonly Cuisine _cuisine;

    public SpecificCuisineSpecification(Cuisine cuisine)
    {
        _cuisine = cuisine;
    }
    
    public override Expression<Func<Recipe, bool>> ToExpression()
        => recipe => recipe.Cuisine == _cuisine;
}

public class SpecificComplexitySpecification : Specification<Recipe>
{
    private readonly Recipe.ComplexityLevel _complexity;

    public SpecificComplexitySpecification(Recipe.ComplexityLevel complexity)
    {
        _complexity = complexity;
    }
    
    public override Expression<Func<Recipe, bool>> ToExpression()
        => recipe => recipe.Complexity == _complexity;
}
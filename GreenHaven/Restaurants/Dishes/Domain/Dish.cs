using GreenHaven.BuildingBlocks;
using GreenHaven.Common;
using GreenHaven.Restaurants.Dishes.Domain.Policies.Review;

namespace GreenHaven.Restaurants.Dishes.Domain;

public sealed class Dish : Entity<Guid>, AggregateRoot<Guid>
{
    public enum Statuses { Initialized, Preparing, Cooked, Served }

    public Guid ChefId { get; private set; }
    
    private readonly List<Guid> _reviewers = new();
    public IReadOnlyCollection<Guid> Reviewers => _reviewers;
    
    public Guid RecipeId { get; private set; }
    
    public DateTime CreatedAt { get; private set; }
    public DateTime? StartedAt { get; private set; }
    public DateTime? FinishedAt { get; private set; }
    public DateTime? ServedAt { get; private set; }
    
    public Statuses Status { get; private set; } = Statuses.Initialized;

    private Dish()
    {
        Id = Guid.NewGuid();
    }

    private Dish(Guid id)
    {
        Id = id;
    }

    public void Prepare(Command.AssignChefCommand command)
    {
        var (chefId, dateTimeProvider) = command;
        ChefId = chefId;
        StartedAt = dateTimeProvider.UtcNow;
        Status = Statuses.Preparing;
    }

    public void Review(Command.ReviewCommand command)
    {
        if (Status != Statuses.Preparing) 
            throw new InvalidOperationException("Wrong status");
        
        var (reviewerId, policy) = command;
        
        _reviewers.Add(reviewerId);

        if (policy.Review(this).IsSuccessful)
        {
            Status = Statuses.Cooked;   
        }
    }

    public void Serve(Command.ServeCommand command)
    {
        if (Status!= Statuses.Cooked)
            throw new InvalidOperationException("Wrong status");
        
        ServedAt = command.DateTimeProvider.UtcNow;
        Status = Statuses.Served;
    }

    public static Dish Create(Command.CreateDishCommand command)
    {
        var dish = new Dish
        {
            RecipeId = command.RecipeId
        };

        return dish;
    }

    public abstract record Command
    {
        public record CreateDishCommand(Guid RecipeId) : Command;
        public record AssignChefCommand(Guid ChefId, IDateTimeProvider DateTimeProvider) : Command;
        public record ReviewCommand(Guid ReviewerId, ReviewPolicy ReviewPolicy) : Command;
        public record ServeCommand(IDateTimeProvider DateTimeProvider) : Command;
    }
}
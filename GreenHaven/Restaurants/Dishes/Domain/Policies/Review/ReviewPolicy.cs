using GreenHaven.BuildingBlocks;

namespace GreenHaven.Restaurants.Dishes.Domain.Policies.Review;

public interface ReviewPolicy
{
    Result Review(Dish dish);
}
using GreenHaven.BuildingBlocks;

namespace GreenHaven.Restaurants.Dishes.Domain.Policies.Review;

public static class ReviewPolicies
{
    public static ReviewPolicy Default => new CompositeReviewPolicy(new NeedNumberOfReviewersPolicy(3));
    
    private class NeedNumberOfReviewersPolicy : ReviewPolicy
    {
        private readonly int _minimumNumberOfReviewers;

        public NeedNumberOfReviewersPolicy(int minimumNumberOfReviewers)
        {
            _minimumNumberOfReviewers = minimumNumberOfReviewers;
        }
        
        public Result Review(Dish dish)
            => dish.Reviewers.Count >= _minimumNumberOfReviewers
              ? Result.Success()
                : Result.Error(new Exception("Not enough reviewers"));
    }
    
    private class AlwaysTruePolicy : ReviewPolicy
    {
        public Result Review(Dish dish)
            => Result.Success();
    }

    private class CompositeReviewPolicy : ReviewPolicy
    {
        private readonly ReviewPolicy[] _reviewPolicies;

        public CompositeReviewPolicy(params ReviewPolicy[] reviewPolicies)
        {
            _reviewPolicies = reviewPolicies;
        }
        
        public Result Review(Dish dish)
            => _reviewPolicies.Select(x => x.Review(dish))
                .Where(x => !x.IsSuccessful)
                .DefaultIfEmpty(Result.Success())
                .First();
    }
}
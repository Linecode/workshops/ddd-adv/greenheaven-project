using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class RecipeGet(IRecipeService recipeService) : EndpointWithoutRequest
{
    public override void Configure()
    {
        Get("/recipes");
        AllowAnonymous();
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var result = await recipeService.GetSpecificRecipes();

        if (!result.Any())
        {
            await SendAsync("", 404, ct);
            return;
        }

        await SendAsync(result, 200, ct);
    }
}
using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class DishPrepare(IDishesService dishesService) : Endpoint<DishPrepareResource>
{
    public override void Configure()
    {
        Post("/dish/{id}/prepare");
        AllowAnonymous();
    }

    public override async Task HandleAsync(DishPrepareResource req, CancellationToken ct)
    {
        var id = Route<Guid>("id");
        
        await dishesService.Prepare(new PrepareDishCommand(id, req.ChefId));

        await SendAsync("", 200, ct);
    }
}

public record DishPrepareResource(Guid ChefId);
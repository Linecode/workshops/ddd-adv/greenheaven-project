using Ardalis.SmartEnum;
using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;
using GreenHaven.Restaurants.Dishes.Domain;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class RecipeCreate(IRecipeService recipeService) : Endpoint<RecipeDto>
{
    public override void Configure()
    {
        Post("/recipe");
        AllowAnonymous();
    }

    public override async Task HandleAsync(RecipeDto req, CancellationToken ct)
    {
        var cuisine = SmartEnum<Cuisine>.FromName(req.Cuisine);
        var complexity = Enum.Parse<Recipe.ComplexityLevel>(req.Complexity);
        
        var id = await recipeService.Create(cuisine, complexity);
        
        HttpContext.Response.Headers["Location"] = $"/api/recipe/{id}";

        await SendAsync("", 201, ct);
    }
}

public record RecipeDto(string Cuisine, string Complexity);
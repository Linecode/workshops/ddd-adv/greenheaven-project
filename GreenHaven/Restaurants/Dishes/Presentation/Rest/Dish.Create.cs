using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class DishCreate(IDishesService dishesService) : Endpoint<DishResource>
{
    public override void Configure()
    {
        Post("/dish");
        AllowAnonymous();
    }

    public override async Task HandleAsync(DishResource req, CancellationToken ct)
    {
        var id = await dishesService.StartPreparation(new StartPreparationCommand(req.RecipeId));
        
        HttpContext.Response.Headers["Location"] = $"/api/dish/{id}";

        await SendAsync("", 201, ct);
    }
}

public record DishResource(Guid RecipeId);
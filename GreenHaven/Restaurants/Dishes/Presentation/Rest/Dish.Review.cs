using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class DishReview(IDishesService dishesService) : Endpoint<DishReviewRequest>
{
    public override void Configure()
    {
        Post("/dish/{id}/accept");
        AllowAnonymous();
    }

    public override async Task HandleAsync(DishReviewRequest req, CancellationToken ct)
    {
        var id = Route<Guid>("id");
        
        await dishesService.AcceptDish(new AcceptDishCommand(id, req.ReviewerId));

        await SendAsync("", 200, ct);
    }
}

public record DishReviewRequest(Guid ReviewerId);
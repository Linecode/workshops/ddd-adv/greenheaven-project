using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class DishServe(IDishesService dishesService) : EndpointWithoutRequest
{
    public override void Configure()
    {
        Post("/dish/{id}/serve");
        AllowAnonymous();
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("id");
        
        await dishesService.Serve(new ServerDishCommand(id));

        await SendAsync("", 200, ct);
    }
}


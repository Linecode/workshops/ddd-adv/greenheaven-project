using FastEndpoints;
using GreenHaven.Restaurants.Dishes.Api;

namespace GreenHaven.Restaurants.Dishes.Presentation.Rest;

public class DishGet(IDishesService dishesService) : EndpointWithoutRequest
{
    public override void Configure()
    {
        Get("/dish/{id}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("id");
        
        var result = await dishesService.GetDish(id);
        
        await SendAsync(result, 200, ct);
    }
}
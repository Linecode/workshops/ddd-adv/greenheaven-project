using GreenHaven.Restaurants.Dishes.Api;
using GreenHaven.Restaurants.Dishes.Api.Ports;
using GreenHaven.Restaurants.Dishes.Infrastructure.Persistence;

namespace GreenHaven.Restaurants.Dishes;

public abstract class DishesModule
{
}

public static class DishesModuleExtensions
{
    public static IServiceCollection AddDishesModule(this IServiceCollection services)
    {
        services.AddScoped<IDishesService, DishesService>();
        services.AddScoped<IRecipeService, RecipeService>();

        services.AddSingleton<IDishesRepository, InMemoryDishRepository>();
        services.AddSingleton<IRecipesRepository, InMemoryRecipesRepository>();
        
        return services;
    }
}
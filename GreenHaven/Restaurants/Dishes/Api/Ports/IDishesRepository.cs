using GreenHaven.Restaurants.Dishes.Domain;

namespace GreenHaven.Restaurants.Dishes.Api.Ports;

public interface IDishesRepository : IDictionary<Guid, Dish>
{
    
}
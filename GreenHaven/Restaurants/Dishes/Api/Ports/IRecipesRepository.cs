using GreenHaven.Restaurants.Dishes.Domain;

namespace GreenHaven.Restaurants.Dishes.Api.Ports;

public interface IRecipesRepository : IDictionary<Guid, Recipe>
{
    
}
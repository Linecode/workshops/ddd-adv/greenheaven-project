using GreenHaven.Restaurants.Dishes.Api.Ports;
using GreenHaven.Restaurants.Dishes.Domain;
using GreenHaven.Restaurants.Dishes.Domain.Specifications;

namespace GreenHaven.Restaurants.Dishes.Api;

public interface IRecipeService
{
    Task<Guid> Create(Cuisine cuisine, Recipe.ComplexityLevel complexity);

    Task<IEnumerable<Recipe>> GetSpecificRecipes();
}

public class RecipeService(IRecipesRepository repository) : IRecipeService
{
    public Task<Guid> Create(Cuisine cuisine, Recipe.ComplexityLevel complexity)
    {
        var recipe = new Recipe(cuisine, complexity);
        
        repository.Add(recipe.Id, recipe);
        
        return Task.FromResult(recipe.Id);
    }

    public Task<IEnumerable<Recipe>> GetSpecificRecipes()
    {
        var specification = new SpecificCuisineSpecification(Cuisine.Italian)
            .And(new SpecificComplexitySpecification(Recipe.ComplexityLevel.Easy));
        
        var recipes = repository.Where(x => specification.IsSatisfiedBy(x.Value)).Select(x => x.Value).ToList();
        
        return Task.FromResult(recipes.AsEnumerable());
    }
}
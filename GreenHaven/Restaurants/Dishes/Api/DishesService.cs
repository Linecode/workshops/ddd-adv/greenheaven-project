using GreenHaven.Common;
using GreenHaven.Restaurants.Dishes.Api.Ports;
using GreenHaven.Restaurants.Dishes.Domain;
using GreenHaven.Restaurants.Dishes.Domain.Policies.Review;

namespace GreenHaven.Restaurants.Dishes.Api;

public interface IDishesService
{
    Task<Guid> StartPreparation(StartPreparationCommand command);
    
    Task<Dish?> GetDish(Guid id);

    Task Prepare(PrepareDishCommand command);

    Task AcceptDish(AcceptDishCommand command);

    Task Serve(ServerDishCommand command);
}

public record StartPreparationCommand(Guid RecipeId);

public record PrepareDishCommand(Guid DishId, Guid ChefId);

public record AcceptDishCommand(Guid DishId, Guid ChefId);

public record ServerDishCommand(Guid DishId);

public class DishesService(
    IDishesRepository dishesRepository, 
    IDateTimeProvider dateTimeProvider) : IDishesService
{
    public Task<Guid> StartPreparation(StartPreparationCommand command)
    {
        var dish = Dish.Create(new Dish.Command.CreateDishCommand(command.RecipeId));
        
        dishesRepository.Add(dish.Id, dish);
        
        return Task.FromResult(dish.Id);
    }

    public Task Prepare(PrepareDishCommand command)
    {
        dishesRepository.TryGetValue(command.DishId, out var dish);
        
        if (dish is null) throw new InvalidOperationException("Dish not found");
        
        dish.Prepare(new Dish.Command.AssignChefCommand(command.ChefId, dateTimeProvider));
        
        return Task.CompletedTask;
    }

    public Task<Dish?> GetDish(Guid id)
    {
        dishesRepository.TryGetValue(id, out var dish);
        return Task.FromResult(dish);
    }

    public Task AcceptDish(AcceptDishCommand command)
    {
        dishesRepository.TryGetValue(command.DishId, out var dish);
        
        if (dish is null) throw new InvalidOperationException("Dish not found");

        // Choose policy
        var policy = ReviewPolicies.Default;
        
        dish.Review(new Dish.Command.ReviewCommand(command.ChefId, policy));
        
        return Task.CompletedTask;
    }

    public Task Serve(ServerDishCommand command)
    {
        dishesRepository.TryGetValue(command.DishId, out var dish);
        
        if (dish is null) throw new InvalidOperationException("Dish not found");
        
        dish.Serve(new Dish.Command.ServeCommand(dateTimeProvider));
        
        return Task.CompletedTask;
    }
}
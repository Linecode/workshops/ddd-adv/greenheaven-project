using GreenHaven.Restaurants.Dishes.Api.Ports;
using GreenHaven.Restaurants.Dishes.Domain;

namespace GreenHaven.Restaurants.Dishes.Infrastructure.Persistence;

public class InMemoryDishRepository : Dictionary<Guid, Dish>, IDishesRepository
{
    
}
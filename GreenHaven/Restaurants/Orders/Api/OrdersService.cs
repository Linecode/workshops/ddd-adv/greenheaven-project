using GreenHaven.Common;
using GreenHaven.Parties.Api;
using GreenHaven.Restaurants.Orders.Domain;
using GreenHaven.Restaurants.Orders.Infrastructure.Persistence;
using GreenHaven.Restaurants.Orders.Infrastructure.Persistence.Mappers;

namespace GreenHaven.Restaurants.Orders.Api;

public record CreateOrderCommand(
    Guid OrderId,
    SalesChannel SalesChannel,
    PartyId Receiver);

// OrderManager
public interface IOrdersService
{
    Task CreateOrder(CreateOrderCommand command);
}

public class OrdersService : IOrdersService
{
    private readonly IOrdersRepository _repository;
    private readonly IOrderFactory _orderFactory;
    private readonly IDateTimeProvider _dateTimeProvider;

    public OrdersService(IOrdersRepository repository, IOrderFactory orderFactory, IDateTimeProvider dateTimeProvider)
    {
        _repository = repository;
        _orderFactory = orderFactory;
        _dateTimeProvider = dateTimeProvider;
    }

    public async Task CreateOrder(CreateOrderCommand command)
    {
        var (id, salesChannel, receiver) = command;
        
        await Handle<InitialOrder>(id, order => OpenOrderDecider.CreateOrder(
            new OpenOrderCommand.OpenCommand(salesChannel, receiver, _dateTimeProvider), order));
    }

    private Task Handle<T>(Guid id, Func<T, Order.OrderEvent> handle) where T : Order
        => _repository.GetAndUpdate(id, (entity) =>
        {
            var aggregate = entity?.MapToAggregate(_orderFactory) ?? GetDefault();
            
            if (aggregate is not T typedOrder) throw new InvalidOperationException();

            var @event = handle(typedOrder);

            return new[] { @event };
        });

    private Order GetDefault() => InitialOrder.Initial;
}
using GreenHaven.Restaurants.Orders.Infrastructure.Persistence;

namespace GreenHaven.Restaurants.Orders.Api;

public interface IOrdersQueryService
{
    Task<OrderEntity?> Get(Guid id);
}

public class OrdersQueryService(IOrdersRepository ordersRepository) : IOrdersQueryService
{
    public async Task<OrderEntity?> Get(Guid id)
    {
        return await ordersRepository.Get(id);
    }
}
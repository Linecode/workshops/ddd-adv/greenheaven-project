using GreenHaven.Restaurants.Orders.Api;
using GreenHaven.Restaurants.Orders.Domain;
using GreenHaven.Restaurants.Orders.Infrastructure.Notifications;
using GreenHaven.Restaurants.Orders.Infrastructure.Persistence;

namespace GreenHaven.Restaurants.Orders;

public abstract class OrdersModule { }

public static class OrdersModuleExtensions
{
    public static IServiceCollection AddOrdersModule(this IServiceCollection services)
    {
        services.AddScoped<IOrdersService, OrdersService>();
        services.AddScoped<IOrdersQueryService, OrdersQueryService>();
        services.AddSingleton<NotificationPublisher>();
        services.AddSingleton<IOrderFactory, Order.Factory>();
        services.AddSingleton<IOrdersRepository, InMemoryOrderRepository>();

        return services;
    }
}
using GreenHaven.Restaurants.Orders.Domain;
using MediatR;
using Order = GreenHaven.Restaurants.Orders.Domain.Order;

namespace GreenHaven.Restaurants.Orders.Infrastructure.Notifications;

public class NotificationPublisher(IPublisher publisher)
{
    public async Task Publish<T>(T @event, Guid entityId) where T : Order.OrderEvent
    {
        var notification = MapFromDomainEvent(@event, entityId);

        if (notification != null)
            await publisher.Publish(notification);
    }

    private static INotification? MapFromDomainEvent(Order.OrderEvent domainEvent, Guid entityId)
    {
        return domainEvent switch
        {
            OpenOrderEvent.OrderOpened @event => new Order.OrderExternalEvent.OrderCreated(entityId,
                @event.SalesChannel),
            _ => null
        };
    }
}
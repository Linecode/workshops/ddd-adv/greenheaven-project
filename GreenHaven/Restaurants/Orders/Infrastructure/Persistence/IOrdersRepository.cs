using GreenHaven.Restaurants.Orders.Domain;

namespace GreenHaven.Restaurants.Orders.Infrastructure.Persistence;

public interface IOrdersRepository
{
    Task GetAndUpdate(Guid id, Func<OrderEntity?, Order.OrderEvent[]> handle);

    // Should be in a separate read repository
    Task<OrderEntity?> Get(Guid id);
}
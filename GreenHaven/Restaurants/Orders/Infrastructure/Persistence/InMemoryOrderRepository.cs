using GreenHaven.Common;
using GreenHaven.Restaurants.Orders.Domain;
using GreenHaven.Restaurants.Orders.Infrastructure.Notifications;

namespace GreenHaven.Restaurants.Orders.Infrastructure.Persistence;

public class InMemoryOrderRepository : Dictionary<Guid, OrderEntity>, IOrdersRepository
{
    private readonly NotificationPublisher _notificationPublisher;

    public InMemoryOrderRepository(NotificationPublisher notificationPublisher)
    {
        _notificationPublisher = notificationPublisher;
    }
    
    public async Task GetAndUpdate(Guid id, Func<OrderEntity?, Order.OrderEvent[]> handle)
    {
        TryGetValue(id, out var orderEntity);
        
        var events = handle(orderEntity);
        
        await ProcessEvents(orderEntity, 
            events.Select(e => new EventEnvelope<Order.OrderEvent>(e, new EventMetadata(id))).ToList());
        
        // dbContext.SaveChangesAsync();
    }

    public Task<OrderEntity?> Get(Guid id)
    {
        TryGetValue(id, out var orderEntity);
        
        return Task.FromResult(orderEntity);
    }

    private async Task ProcessEvents(OrderEntity?  entity, IEnumerable<EventEnvelope<Order.OrderEvent>>? events)
    {
        foreach (var eventEnvelope in events)
        {
            Evolve(entity, eventEnvelope);
            // Good place for processing events with outbox pattern
            await _notificationPublisher.Publish(eventEnvelope.Event, eventEnvelope.Metadata.RecordId);
        }
    }

    private void Evolve(OrderEntity? entity, EventEnvelope<Order.OrderEvent> eventEnvelope)
    {
        var @event = eventEnvelope.Event;
        var id = eventEnvelope.Metadata.RecordId;

        switch (@event)
        {
            case OpenOrderEvent.OrderOpened (var salesChannel, var receiver, var createdAt):
                Add(id, new OrderEntity
                {
                    Id = id,
                    SalesChannel = (OrderEntity.SalesChannels)salesChannel,
                    Receiver = receiver.ToGuid(),
                    CreatedAt = createdAt
                });
                break;
        }
    }
}
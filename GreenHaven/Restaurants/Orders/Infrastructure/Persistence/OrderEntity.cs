using GreenHaven.Restaurants.Orders.Domain;

namespace GreenHaven.Restaurants.Orders.Infrastructure.Persistence;

public class OrderEntity
{
    public enum State { Open, Closed, Cancelled }
    public enum SalesChannels { Restaurant, Bar, UberEats, Glovo  }
    
    public required Guid Id { get; set; }
    public State CurrentStatus { get; set; } = State.Open;
    
    public DateTime CreatedAt { get; set; }
    public SalesChannels SalesChannel { get; set; }
    public Guid Receiver { get; set; }
    
    public IEnumerable<OrderLineEntity> OrderLines { get; set; } = new List<OrderLineEntity>();
    public IEnumerable<ChargeLineEntity> ChargeLines { get; set; } = new List<ChargeLineEntity>();
}

public class OrderLineEntity
{
    public required Guid Id { get; set; }
    public Guid ProductType { get; set;  }
    public string Description { get; set; }
    public string? Comment { get; set; }
    public int NumberOrdered { get; set; }
    public decimal UnitPrice { get; set; }
    public string UnitCurrency { get; set; }
    public IEnumerable<TaxOnLineEntity> Taxes { get; set; } = new List<TaxOnLineEntity>();
}

public class TaxOnLineEntity
{
    public decimal Rate { get; set; }
    public string Type { get; set; }
}

public class ChargeLineEntity
{
    public decimal Amount { get; set; }
    public string AmountCurrency { get; set; } = null!;
    public string Description { get; set;  } = null!;
    public string? Comment { get; set;  }
    
    public TaxOnLine Tax { get; set;  } = null!;
}
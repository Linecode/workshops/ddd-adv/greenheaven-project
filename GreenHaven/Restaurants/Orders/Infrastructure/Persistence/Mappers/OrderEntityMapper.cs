using GreenHaven.Restaurants.Orders.Domain;

namespace GreenHaven.Restaurants.Orders.Infrastructure.Persistence.Mappers;

public static class OrderEntityMapper
{
    public static Order MapToAggregate(this OrderEntity orderEntity, IOrderFactory factory)
        => factory.Create(
            orderEntity.Id, 
            (Order.State) orderEntity.CurrentStatus);
}
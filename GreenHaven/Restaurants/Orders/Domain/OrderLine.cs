using EnsureThat;
using GreenHaven.BuildingBlocks;
using GreenHaven.Currency.Api;

namespace GreenHaven.Restaurants.Orders.Domain;

public sealed class OrderLine : Entity<Guid>
{
    public Guid ProductType { get; }
    public string Description { get; } = null!;
    public string? Comment { get; }
    public int NumberOrdered { get; private set; }
    public Money UnitPrice { get; } = null!;
    
    private readonly List<TaxOnLine> _taxes = new();
    public IReadOnlyList<TaxOnLine> Taxes => _taxes;
    
    //We can define parties here if we need to support multiple receivers but for restaurant it doesn't make sense

    private OrderLine() {}

    public OrderLine(Guid id, Guid productType, string description, string? comment,
        int numberOrdered, Money unitPrice)
    {
        Ensure.That(numberOrdered).IsGt(0);
        Ensure.That(productType).IsNot(Guid.Empty);
        
        ArgumentNullException.ThrowIfNull(unitPrice);
        
        Id = id;
        ProductType = productType;
        Description = description;
        Comment = comment;
        NumberOrdered = numberOrdered;
        UnitPrice = unitPrice;
    }

    public OrderLine(Guid productType, string description, string? comment,
        int numberOrdered,
        Money unitPrice) : this(Guid.NewGuid(),
        productType,
        description,
        comment,
        numberOrdered,
        unitPrice)
    {
    }
    
    // Actions / Behaviors
    
    public void IncreaseNumberOrdered(int number) {
        NumberOrdered += number;
    }
    
    public void DecreaseNumberOrdered(int number) {
        NumberOrdered -= number;
    }
    
    public void AddTax(TaxOnLine tax) {
        _taxes.Add(tax);
    }
    
    public void RemoveTax(TaxOnLine tax) {
        _taxes.Remove(tax);
    }

    public Money CalculateLinePrice()
    {
        var result = UnitPrice;

        foreach (var tax in _taxes)
        {
            result += tax.CalculateTax(UnitPrice);
        }

        return result;
    }
}
using GreenHaven.Common;
using GreenHaven.Currency.Api;
using GreenHaven.Parties.Api;
using MediatR;

namespace GreenHaven.Restaurants.Orders.Domain;

public interface IOrderFactory
{
    public Order Create(Guid orderId, Order.State state);
}

public abstract record Order(
    IEnumerable<OrderLine> OrderLines,
    IEnumerable<ChargeLine> ChargeLines)
{
    public enum State { Open, Closed, Cancelled }
    
    // Common behavior
    public Money CalculateTotalPrice()
    {
        Money result = Money.Zero(OrderLines.First().UnitPrice.Currency);
        
        foreach (var line in OrderLines)
        {
            result += line.CalculateLinePrice();
        }

        foreach (var line in ChargeLines)
        {
            result += line.CalculatePrice();
        }

        return result;
    }
    
    public class Factory : IOrderFactory
    {
        public Order Create(Guid orderId, State state)
        {
            throw new NotImplementedException();
        }
    }

    public abstract record OrderCommand;

    public abstract record OrderEvent;

    // For Choreography
    public abstract record OrderExternalEvent
    {
        public record OrderCreated(Guid OrderId, SalesChannel SalesChannel) : OrderExternalEvent, INotification;
    };
}

public record InitialOrder() : Order(Array.Empty<OrderLine>(), Array.Empty<ChargeLine>())
{
    public static InitialOrder Initial = new();
}

public record OpenedOrder(
    DateTime CreatedAt,
    SalesChannel SalesChannel,
    PartyId Receiver,
    IEnumerable<OrderLine> OrderLines,
    IEnumerable<ChargeLine> ChargeLines) : Order(OrderLines, ChargeLines)
{
}

public abstract record OpenOrderCommand : Order.OrderCommand
{
    public record OrderCommand(OrderLine OrderLine) : OpenOrderCommand;
    public record CancelOrderCommand(Guid OrderLineId) : OpenOrderCommand;
    public record AddChargeLine(ChargeLine ChargeLine) : OpenOrderCommand;
    public record RemoveChargeLine(Guid ChargeLineId) : OpenOrderCommand;
    public record ChangeReceiver(PartyId Receiver) : OpenOrderCommand;

    public record CloseOrderCommand : OpenOrderCommand;
    public record OpenCommand(
        SalesChannel SalesChannel,
        PartyId Receiver,
        IDateTimeProvider DateTimeProvider) : OpenOrderCommand;
}

public abstract record OpenOrderEvent : Order.OrderEvent
{
    // Should be mapped to properties
    public record NewOrderLineAdded(OrderLine OrderLine) : OpenOrderEvent;

    public record OrderClosed() : OpenOrderEvent;

    public record OrderOpened(
        SalesChannel SalesChannel, PartyId Receiver, DateTime CreatedAt) : OpenOrderEvent;
}

public static class OpenOrderDecider
{
    public static OpenOrderEvent CreateOrder(OpenOrderCommand.OpenCommand command, InitialOrder _)
    {
        var (salesChannel, receiver, dateTimeProvider) = command;
        return new OpenOrderEvent.OrderOpened(salesChannel, receiver, dateTimeProvider.UtcNow);
    }
    
    public static OpenOrderEvent AddOrderLine(OpenOrderCommand.OrderCommand command, OpenedOrder state)
    {
        // invariant goes here
        var currentOrderPrice = state.CalculateTotalPrice();
        var orderLinePrice = command.OrderLine.CalculateLinePrice();
        var newOrderPrice = currentOrderPrice + orderLinePrice;
        
        if (newOrderPrice >= Money.New(10000, newOrderPrice.Currency))
            throw new InvalidOperationException($"Cannot add more than 10000 {newOrderPrice.Currency} to an order");

        return new OpenOrderEvent.NewOrderLineAdded(command.OrderLine);
    }

    public static OpenOrderEvent CloseOrder(OpenOrderCommand.CloseOrderCommand command, OpenedOrder state)
    {
        return new OpenOrderEvent.OrderClosed();
    }
}
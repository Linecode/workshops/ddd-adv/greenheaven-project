using GreenHaven.BuildingBlocks;
using GreenHaven.Currency.Api;

namespace GreenHaven.Restaurants.Orders.Domain;

// Extra Charges, like e.g. add 10% service fee or for special request
public class ChargeLine : ValueObject<ChargeLine>
{
    public Money Amount { get; } = null!;
    public string Description { get; } = null!;
    public string? Comment { get; }
    
    public TaxOnLine Tax { get; } = null!;

    private ChargeLine() {}
    
    public ChargeLine(Money amount, string description, string? comment, TaxOnLine tax)
    {
        if (amount <= Money.Zero(amount.Currency))
        {
            throw new ArgumentOutOfRangeException(nameof(amount));
        }

        ArgumentNullException.ThrowIfNull(tax);
        
        Amount = amount;
        Description = description;
        Comment = comment;
        Tax = tax;
    }

    public Money CalculatePrice()
    {
        return Amount + Tax.CalculateTax(Amount);
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Amount;
        yield return Description;
        yield return Comment!;
    }
}
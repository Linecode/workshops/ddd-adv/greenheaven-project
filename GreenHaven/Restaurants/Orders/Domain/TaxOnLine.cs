using GreenHaven.BuildingBlocks;
using GreenHaven.Currency.Api;

namespace GreenHaven.Restaurants.Orders.Domain;

public class TaxOnLine : ValueObject<TaxOnLine>
{
    decimal Rate { get; }
    string Type { get; }

    public TaxOnLine(decimal rate, string type)
    {
        Rate = rate;
        Type = type;
    }

    public Money CalculateTax(Money amount)
    {
        return amount * Rate;
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Rate;
        yield return Type;
    }
}
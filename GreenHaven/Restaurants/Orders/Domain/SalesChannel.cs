namespace GreenHaven.Restaurants.Orders.Domain;

public enum SalesChannel { Restaurant, Bar, UberEats, Glovo }
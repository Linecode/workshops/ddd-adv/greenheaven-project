using FastEndpoints;
using GreenHaven.Restaurants.Orders.Api;

namespace GreenHaven.Restaurants.Orders.Presentation.Rest;

public class OrderGet(IOrdersQueryService ordersQueryService) : EndpointWithoutRequest
{
    public override void Configure()
    {
        Get("/order/{id}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("id");
        
        var result = await ordersQueryService.Get(id);
        
        await SendAsync(result, 200, ct);
    }
}
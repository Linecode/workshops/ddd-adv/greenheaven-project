using FastEndpoints;
using GreenHaven.Parties.Api;
using GreenHaven.Restaurants.Orders.Api;
using GreenHaven.Restaurants.Orders.Domain;

namespace GreenHaven.Restaurants.Orders.Presentation.Rest;

public class OrderCreate(IOrdersService ordersService) : Endpoint<PartyId>
{
    public override void Configure()
    {
        Post("/order");
        AllowAnonymous();
    }

    public override async Task HandleAsync(PartyId req, CancellationToken ct)
    {
        var id = Guid.NewGuid();
        var salesChannel = Enum.Parse<SalesChannel>(HttpContext.Request.Headers["X-GH-SalesChannel"]!);
        
        var command = new CreateOrderCommand(id, salesChannel, req);
        
        await ordersService.CreateOrder(command);
        
        HttpContext.Response.Headers["Location"] = $"/api/order/{id}";

        await SendAsync("", 201, ct);
    }
}

public record OrderCreateResource(Guid PartyId);
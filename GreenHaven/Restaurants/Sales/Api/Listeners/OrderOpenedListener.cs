using GreenHaven.Restaurants.Orders.Domain;
using MediatR;

namespace GreenHaven.Restaurants.Sales.Api.Listeners;

public class OrderOpenedListener : INotificationHandler<Order.OrderExternalEvent.OrderCreated>
{
    public Task Handle(Order.OrderExternalEvent.OrderCreated notification, CancellationToken cancellationToken)
    {
        Console.WriteLine($"Order {notification.OrderId} was opened by {notification.SalesChannel}");
        
        // Here we should call applications service
        
        return Task.CompletedTask;
    }
}
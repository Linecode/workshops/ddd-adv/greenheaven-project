using GreenHaven.Parties.Api;
using OpenSleigh.Core;
using OpenSleigh.Core.Messaging;

namespace GreenHaven.Restaurants.BusinessProcess.RestaurantPreparation;

// Orchestration
public class RestaurantPreparationSaga : Saga<RestaurantPreparationSagaState>,
    IStartedBy<StartRestaurantPreparationCommand>,
    IHandleMessage<CleanRestaurant>,
    IHandleMessage<SetupTables>,
    IHandleMessage<RestaurantPrepared>
{
    // We can inject application services from the DI container
    public RestaurantPreparationSaga(RestaurantPreparationSagaState state) : base(state)
    {
    }

    public Task HandleAsync(IMessageContext<StartRestaurantPreparationCommand> context, CancellationToken cancellationToken = new())
    {
        // here we should call application service
        State.Cleaner = new PartyId(Guid.NewGuid());
        
        Publish(new CleanRestaurant(Guid.NewGuid(), context.Message.CorrelationId, State.Cleaner));

        return Task.CompletedTask;
    }

    public Task HandleAsync(IMessageContext<CleanRestaurant> context, CancellationToken cancellationToken = new())
    {
        State.Waiter = new PartyId(Guid.NewGuid());
        
        Publish(new SetupTables(Guid.NewGuid(), context.Message.CorrelationId, State.Waiter));

        return Task.CompletedTask;
    }

    public Task HandleAsync(IMessageContext<SetupTables> context, CancellationToken cancellationToken = new())
    {
        Publish(new RestaurantPrepared(Guid.NewGuid(), context.Message.CorrelationId));

        return Task.CompletedTask;
    }

    public Task HandleAsync(IMessageContext<RestaurantPrepared> context, CancellationToken cancellationToken = new())
    {
        State.MarkAsCompleted();

        return Task.CompletedTask;
    }
}
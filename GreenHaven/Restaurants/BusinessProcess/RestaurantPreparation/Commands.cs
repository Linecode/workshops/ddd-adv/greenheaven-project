using GreenHaven.Parties.Api;
using OpenSleigh.Core.Messaging;

namespace GreenHaven.Restaurants.BusinessProcess.RestaurantPreparation;

public record StartRestaurantPreparationCommand(Guid Id, Guid CorrelationId) : ICommand;

public record CleanRestaurant(Guid Id, Guid CorrelationId, PartyId Waitress) : ICommand;
public record SetupTables(Guid Id, Guid CorrelationId, PartyId Waitress) : ICommand;

public record RestaurantPrepared(Guid Id, Guid CorrelationId) : ICommand;
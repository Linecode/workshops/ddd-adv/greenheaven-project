using GreenHaven.Parties.Api;
using OpenSleigh.Core;

namespace GreenHaven.Restaurants.BusinessProcess.RestaurantPreparation;

public record RestaurantPreparationSagaState : SagaState
{
    public RestaurantPreparationSagaState(Guid id) : base(id)
    {
    }

    public PartyId Cleaner { get; set; } = null!;
    public PartyId Waiter { get; set; } = null!;
}